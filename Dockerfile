FROM perl:5.26

ADD . /src
WORKDIR /src
RUN cpanm .

ENTRYPOINT [ "esa" ]
