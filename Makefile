VERSION = v0.2.0

env:
	docker-compose -p esa -f examples/elasticsearch/docker-compose.yml up -d elasticsearch

build:
	docker build -t vredens/esa .

push:
	docker tag vredens/esa:latest vredens/esa:$(VERSION)
	docker push vredens/esa:$(VERSION)

push-latest:
	docker push vredens/esa:latest
