use v5.10;

=pod

ESA_main functions must return an integer value with the following meaning

If 0 the document is unchanged and is valid for indexing.
If 1 the document was changed and is valid for indexing.
If -1 the document is not valid for indexing and should be discarded.

You can use your ESA_main to store invalid documents somewhere if needed.

The document itself is always passed as a hashref and any changes should be made
to the same hashref.

The C<meta> contains a hashref with

  {
    _type => 'type',
    _id => 'id'
  }

which should be used as read only parameters and to decide what type of 
manipulation should be done to the document.

=cut
sub ESA_main {
	my ($meta, $doc) = @_;
	
	my $r = 0;
	
	if ($meta->{_type} eq 'user') {
		return -1 unless defined $doc->{name};
	
		unless (defined $doc->{'last-name'}) {
			my @names = split /\s+/, $doc->{'name'};
			shift @names;
			$doc->{'last-name'} = join (' ', @names);
			$r = 1;
		}
	}
	
	return $r;
}

1;
