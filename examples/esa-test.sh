#!/bin/bash

basedir="$(dirname $(perl -e 'use Cwd qw/abs_path/; print abs_path($ARGV[0]);' $0))/.."
cd $basedir

ES_HOST=${ES_HOST:-localhost:9200}
SRC_INDEX=${SRC_INDEX:-tweetapp}
DST_INDEX=${DST_INDEX:-tweetapp-v1}

set -e

case "$1" in
	'init')
		./scripts/esa create --host "$ES_HOST" --index $SRC_INDEX --type user --mfolder ./examples/mappings/sample/
		./scripts/esa create --host "$ES_HOST" --index $SRC_INDEX --type tweet --mfolder ./examples/mappings/sample/
		./scripts/esa restore --host "$ES_HOST" --index $SRC_INDEX --folder ./examples/backups/
	;;
	
	'reindex')
		./scripts/esa create --host "$ES_HOST" --index $DST_INDEX --type user --mfolder ./examples/mappings/sample/
		./scripts/esa create --host "$ES_HOST" --index $DST_INDEX --type tweet --mfolder ./examples/mappings/sample/
		./scripts/esa set --host "$ES_HOST" --index $SRC_INDEX --no-writes
		./scripts/esa set --host "$ES_HOST" --index $DST_INDEX --refresh-interval -1
		./scripts/esa reindex --host "$ES_HOST" --src-index $SRC_INDEX --dst-index $DST_INDEX --type user --plugin ./examples/esa-plugin.pl
		./scripts/esa reindex --host "$ES_HOST" --src-index $SRC_INDEX --dst-index $DST_INDEX --type tweet --plugin ./examples/esa-plugin.pl
		./scripts/esa set --host "$ES_HOST" --index $SRC_INDEX --writes
		./scripts/esa set --host "$ES_HOST" --index $DST_INDEX --refresh-interval 1
	;;
	
	'full-reindex')
		./scripts/esa create --host "$ES_HOST" --index $DST_INDEX --type user --mfolder ./examples/mappings/sample/
		./scripts/esa create --host "$ES_HOST" --index $DST_INDEX --type tweet --mfolder ./examples/mappings/sample/
		./scripts/esa set --host "$ES_HOST" --index $SRC_INDEX --no-writes
		./scripts/esa set --host "$ES_HOST" --index $DST_INDEX --refresh-interval -1
		./scripts/esa reindex --host "$ES_HOST" --src-index $SRC_INDEX --dst-index $DST_INDEX --plugin ./examples/esa-plugin.pl
		./scripts/esa set --host "$ES_HOST" --index $SRC_INDEX --writes
		./scripts/esa set --host "$ES_HOST" --index $DST_INDEX --refresh-interval 1
	;;
	
	'repair')
		./scripts/esa create  --host "$ES_HOST" --index $DST_INDEX --type user --type event --mfolder mappings/events/
		./scripts/esa create  --host "$ES_HOST" --index $DST_INDEX --type tweet --mfolder mappings/events/
		./scripts/esa set     --host "$ES_HOST" --index $DST_INDEX --refresh-interval -1
		./scripts/esa reindex --host "$ES_HOST" --index $DST_INDEX --plugin test-esa.pl
		./scripts/esa set     --host "$ES_HOST" --index $DST_INDEX --refresh-interval 1
	;;
	
	*)
		echo "Usage: $0 init|reindex|repair"
		echo
		echo "ENV Vars"
		echo "  ES_HOST=$ES_HOST"
		echo "  SRC_INDEX=$SRC_INDEX"
		echo "  DST_INDEX=$DST_INDEX"
	;;
esac
