#!/bin/bash

old_version=0.1.0
new_version=0.2.0

set -e

perl -pi -e "s/^Version ${old_version}/Version ${new_version}/s" scripts/esa lib/ESA.pm
perl -pi -e "s/VERSION = '${old_version}';/VERSION = '${new_version}';/s" lib/ESA.pm
perl -pi -e "s/VERSION = v${old_version}/VERSION = v${new_version}/s" Makefile

pod2markdown scripts/esa > README.md

perl Build.PL
./Build manifest

