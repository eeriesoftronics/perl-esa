#!/usr/bin/env perl

# devmode libs
use lib './extlib/lib/perl5';
use lib './lib';
use v5.10;

use Data::Dumper;
use Getopt::Long;
use Pod::Usage;
use File::Path qw/make_path/;
use File::Spec;
use Term::ANSIColor ':constants';

use Log::Any::Adapter;
use Log::Dispatch;
use JSON::XS;
use Search::Elasticsearch;
use SemVer;

use ESA;

#my $log = Log::Dispatch->new(outputs => [[ 'File', min_level => 'info', filename => './esa.log' ]]);
#Log::Any::Adapter->set( 'Dispatch', dispatcher => $log );

Log::Any::Adapter->set( 'File', './esa.log', log_level => 'info' );

my $opts = {
	'docs-per-file' => 1000,
	'host' => 'localhost:9200',
	'verbose' => 0,
};

GetOptions($opts,
	# backup and restore options
	'index=s',
	'type=s',
	'folder=s',
	'plugin=s',
	'from=i',

	# indice creation options
	'mfolder=s',

	# reindex options
	'src-index=s',
	'src-type=s',
	'dst-index=s',
	'dst-type=s',

	'docs-per-file=s',
	'ignore-conflict',

	# alias
	'alias=s',
	'query=s',

	#settings
	'writes!',
	'replicas=s',
	'refresh-interval=s',

	'host=s',
	'dst-host=s',

	'verbose',

	'man',
	'help',
);

pod2usage({-exitval => 0, verbose => 2}) if $opts->{man};
pod2usage({-exitval => 0, verbose => 1}) if $opts->{help};
die 'invalid hostname format, use <hostname/IP>:<port>' unless defined $opts->{host} and $opts->{host} =~ m!(https?://)? ([-_\w\d\.]+)? (:[-_\w\d\.]+)? [-_\w\d\.]+ : \d{1,5}$!x;
die 'invalid docs-per-file format. must be a number from 100 to 10000' unless $opts->{'docs-per-file'} >= 100 and $opts->{'docs-per-file'} <= 10000;
$opts->{'dst-host'} = $opts->{'host'} unless defined $opts->{'dst-host'};
die 'invalid dst-host format, use <hostname/IP>:<port>' unless defined $opts->{'dst-host'} and $opts->{'dst-host'} =~ m!(https?://)? ([-_\w\d\.]+)? (:[-_\w\d\.]+)? [-_\w\d\.]+ : \d{1,5}$!x;

my $es = Search::Elasticsearch->new(nodes => [ $opts->{host} ]);

my $actions = {
	'backup' => \&backup,
	'bu' => \&backup,
	'restore' => \&restore,
	're' => \&restore,
	'reindex' => \&reindex,
	'ri' => \&reindex,
	'create' => \&create_index,
	'cr' => \&create_index,
	'set' => \&set_settings,
	'info' => \&info,
	# alias manipulation
	'alias' => \&alias,
};

my $action = shift;

pod2usage() unless defined $actions->{$action};

&{$actions->{$action}}(@ARGV);

#===============================================================================
# Graceful dies
#===============================================================================
BEGIN {
	$SIG{__DIE__} = sub {
		if ($_[0] =~ /^usage error/) {
			die RED, @_, RESET, "\nUse $0 --man\n";
		}
	};
}

#===============================================================================
sub set_settings {
#===============================================================================
	my $index = $opts->{index};

	die 'usage error: ', 'settings is missing parameters' unless defined $index;

	my $settings = {};

	if (defined $opts->{writes}) {
		$settings->{index} = {} unless defined $settings->{index};
		$settings->{index}->{blocks} = {} unless defined $settings->{index}->{blocks};
		$settings->{index}->{blocks}->{write} = $opts->{writes} ? 'false' : 'true';
	}

	if (defined $opts->{replicas}) {
		die 'invalid replicas value, must be a value between 0 and 5' unless $opts->{replicas} =~ /^\d+$/ and $opts->{replicas} >= 0 and $opts->{replicas} <= 5;
		$settings->{index} = {} unless defined $settings->{index};
		$settings->{index}->{number_of_replicas} = $opts->{replicas};
	}

	if (defined $opts->{'refresh-interval'}) {
		die 'invalid refresh-interval value, must be either -1 or a value between 1 and 30' unless
				$opts->{'refresh-interval'} =~ /^-?\d+$/ and
				(
					$opts->{'refresh-interval'} == -1 or
					($opts->{'refresh-interval'} >= 1 and $opts->{'refresh-interval'} <= 30)
				);
		$settings->{index} = {} unless defined $settings->{index};
		$settings->{index}->{refresh_interval} = $opts->{'refresh-interval'} . ($opts->{'refresh-interval'} == -1 ? '' : 's');
	}

	die 'usage error: you forgot to specify at least one setting to change' unless keys %$settings;

	my $res = $es->indices->put_settings({
		index => $index,
		body => $settings
	}) or die 'failed to update settings';

	say '[', $index, '] settings updated';
	say BLUE, to_pretty_json($res), RESET;
}

#===============================================================================
sub info {
#===============================================================================
	my $index = $opts->{index};

	die 'usage error: missing index' unless defined $index;

	my $res = $es->indices->get_settings({
		index => $index,
	}) or die 'failed to update settings';

	say 'index settings:';
	say BLUE, to_pretty_json($res), RESET;
}

#===============================================================================
sub scan {
#===============================================================================
	my $index = shift;
	my $type = shift;
	my $method = shift;

	my $scroll = $es->scroll_helper(
		index       => $index,
		type        => $type,
		size        => 100
	);

	while (my $doc = $scroll->next) {
		&$method($doc);
	}
}

#===============================================================================
sub backup {
#===============================================================================
	my $index = $opts->{index};
	my $type = $opts->{type};
	my $folder = $opts->{folder};

	die 'usage error: ', 'backup is missing required parameters' unless defined $index and defined $folder;

	make_path $folder unless -d $folder;

	my $dcnt = 0;
	my $fcnt = 0;
	my $fh;

	my $file_basename = defined $type ? "$index.$type-%05d.json" : "$index-%05d.json";

	scan($index, $type, sub {
		my $doc = shift;

		if ($dcnt % $opts->{'docs-per-file'} == 0) {
			close($fh) if defined $fh;
			say 'Written chunk: ', $fcnt, '. Documents per chunk: ', $opts->{'docs-per-file'}, '. Documents written so far: ', $dcnt;
			open($fh, '>', File::Spec->catfile($folder, sprintf($file_basename, $fcnt++)));
		}
		my $head = {
			index => {
				_index => $doc->{_index},
				_type => $doc->{_type},
				_id => $doc->{_id},
			}
		};

		print $fh to_json($head), "\n";
		print $fh to_json($doc->{_source}), "\n";
		$dcnt++;
	});

	say 'Scanned through ', $dcnt, ' documents and created ', $fcnt, ' files';

	close($fh);
}

#===============================================================================
sub restore {
#===============================================================================
	my $src_index = $opts->{'src-index'} || $opts->{'index'};
	my $dst_index = $opts->{'dst-index'} || $opts->{'index'};
	my $from = $opts->{'from'} || 0;
	my $type = $opts->{type};
	my $folder = $opts->{folder};

	die 'usage error: restore requires some mandatory parameters' unless defined $src_index and defined $dst_index and defined $folder;

	die 'no such folder ', $folder unless -d $folder;

	my $cnt = $from // 0;
	my $file_basename = defined $type ? "$src_index.$type-%05d.json" : "$src_index-%05d.json";
	my $file = File::Spec->catfile($folder, sprintf($file_basename, $cnt));

	die RED, "No such file [$file]", RESET unless -f $file;

	if ($opts->{plugin}) {
		require $opts->{plugin};
		die 'your plugin contains no ESA_main function' unless exists &ESA_main;
	}

	my $bulk = $es->bulk_helper(
		index   => $dst_index,
		on_success => $opts->{'verbose'} ? sub {
			my ($action,$response,$i) = @_;
			say BLUE, 'bulk action [', $i, ':', $action, '] done: ', to_json($response), RESET;
		} : undef,
		on_error => sub {
			my ($action,$response,$i) = @_;
			die RED, 'bulk action [', $i, ':', $action, '] failed:', to_json($response), RESET;
		},
		on_conflict => sub {
			my ($action,$response,$i,$version) = @_;
			if ($opts->{'ignore-conflict'}) {
				warn RED, 'bulk action [', $i, ':', $action, '][version:', $version, '] failed (conflict):', to_json($response), RESET;
			} else {
				die RED, 'bulk action [', $i, ':', $action, '][version:', $version, '] failed (conflict):', to_json($response), RESET;
			}
		},
	);

	while (-f $file) {
		say GREEN, "  > Reading file [$file]", RESET;
		open (my $fh, '<', $file) or die 'failed to open ', $file;
		my @data = <$fh>;

		for (my $i = 0; $i < @data; $i += 2) {
			my $head = decode_json($data[$i]);
			my $ihead = $head->{index};
			my $doc = decode_json($data[$i+1]);

			if (exists &ESA_main) {
				next if ESA_main($ihead, $doc) == -1;
			}

			$ihead->{_type} = $type if defined $type;

			$bulk->index({
				type   => $ihead->{_type},
				id     => $ihead->{_id},
				parent => $ihead->{parent},
				source => $doc
			});
		}

		close($fh);

		$file = File::Spec->catfile($folder, sprintf($file_basename, ++$cnt));
	}

	$bulk->flush();
}

#===============================================================================
sub reindex {
#===============================================================================
	my $dst_host = $opts->{'dst-host'} || $opts->{'host'};
	my $src_index = $opts->{'src-index'} || $opts->{'index'};
	my $dst_index = $opts->{'dst-index'} || $opts->{'index'};
	my $type = $opts->{type};

	my $dst_es = ($dst_host eq $opts->{'host'} ? $es : Search::Elasticsearch->new(nodes => [ $dst_host ]));

	die 'usage error: reindex requires some mandatory parameters' unless defined $src_index and defined $dst_index;

	#TODO: https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-reindex.html

	# set flag to indicate if same index or not
	my $same = 0;
	if ($src_index eq $dst_index and $dst_host eq $opts->{'host'}) {
		$same = 1;
		say YELLOW, 'reindexing into the same index', RESET;
	}

	if ($opts->{plugin}) {
		require $opts->{plugin};
		die 'your code contains no ESA_main function' unless exists &ESA_main;
	}

	my $version_type = 'external';
	$version_type = 'internal' if $src_index eq $dst_index;

	my $cnt = 0;
	my $bulk = $dst_es->bulk_helper(
		index   => $dst_index,
		type    => $type,
		on_success => $opts->{'verbose'} ? sub {
			my ($action,$response,$i) = @_;
			say BLUE, 'bulk action [', $i, ':', $action, '] done: ', to_json($response), RESET;
		} : undef,
		on_error => sub {
			my ($action,$response,$i) = @_;
			die RED, 'bulk action [', $i, ':', $action, '] failed:', to_json($response), RESET;
		},
		on_conflict => sub {
			my ($action,$response,$i,$version) = @_;
			if ($opts->{'ignore-conflict'}) {
				warn YELLOW, 'bulk action [', $i, ':', $action, '][version:', $version, '] failed (conflict):', to_json($response), RESET;
			} else {
				die RED, 'bulk action [', $i, ':', $action, '][version:', $version, '] failed (conflict):', to_json($response), RESET;
			}
		},
	);

	my $dcnt = 0;
	scan($src_index, $type, sub {
		my $doc = shift;

		if ($same) {
			if (exists &ESA_main) {
				return undef if ESA_main($doc, $doc->{_source}) < 1;
			} else {
				# same index, nothing to do unless we have a plugin to change docs
				# TODO: add a flag to force reindexing all docs regardless
				return undef;
			}
		} else {
			if (exists &ESA_main) {
				return undef if ESA_main($doc, $doc->{_source}) == -1;
			}
		}

		$bulk->index({
			type   => $doc->{_type},
			id     => $doc->{_id},
			parent => $doc->{parent},
			source => $doc->{_source}
		});

		$dcnt++;

		say BLUE, 'Reindexed ', $dcnt, ' documents so far', RESET unless $dcnt % 1000;
	});

	$bulk->flush();

	say GREEN, 'Reindexed ', $dcnt, ' documents', RESET;
}

#===============================================================================
sub create_index {
#===============================================================================
	my $index = $opts->{index};
	my $type = $opts->{type};
	my $mfolder = $opts->{'mfolder'};

	die 'usage error: missing index' unless defined $index;
	die 'usage error: missing mfolder' unless defined $mfolder;

	my $settings;
	my $dst_mappings;

	if (-d $mfolder) {
		#die 'usage error: missing type' unless defined $type;
		die 'could not find mappings folder ', $mfolder unless -d $mfolder;
		my $fsettings = File::Spec->catfile($mfolder, 'settings.json');
		my $fmappings = File::Spec->catfile($mfolder, 'mappings.json');

		my $fh;
		local $/='';
		open ($fh, '<', $fsettings) or die 'failed to load indice settings from file ', $fsettings;
		$settings = <$fh>;
		close($fh);

		open ($fh, '<', $fmappings) or die 'failed to load indice mappings from file ', $fmappings;
		$dst_mappings = <$fh>;
		close($fh);

		$settings = decode_json($settings);
		$dst_mappings = decode_json($dst_mappings);
	} elsif (-f $mfolder) {
		my $fh;
		local $/='';
		open ($fh, '<', $mfolder) or die 'failed to load indice settings and mappings from file ', $mfolder;
		my $idx_config = <$fh>;
		close($fh);

		my $idx_config = decode_json($idx_config);

		die 'invalid index configuration loaded from [', $mfolder, ']' unless defined $idx_config->{settings};
		die 'invalid index configuration loaded from [', $mfolder, ']' unless defined $idx_config->{mappings};

		$settings = $idx_config->{settings};
		$dst_mappings = $idx_config->{mappings};
	}	else {
		die 'invalid mfolder [', $mfolder, '], must be a file or folder.';
	}

	my @types = $type ? ($type) : keys %{$dst_mappings || {}};
	if (scalar(@types) == 0) {
		say 'no types to install, skipping everything';
		return 1;
	}

	# check index exists
	unless ($es->indices->exists(index => $index)) {
		# create the index
		say 'index does not exist, creating [', $index, ']';
		$es->indices->create(index => $index, body => { settings => $settings }) or die 'failed to create index ', $index;
	}

	# check mapping exists
	my $src_mappings = $es->indices->get_mapping(index => $index)->{$index}->{'mappings'};

	# compare src mapping to dst mapping
	for my $type (@types) {
		unless (defined $dst_mappings->{$type}) {
			say 'no such type in the desired mapping, ignoring [type:', $type, ']';
			next;
		}
		my $comparison = mappings_comparator($src_mappings->{$type}->{_meta}->{version}, $dst_mappings->{$type}->{_meta}->{version});
		if ($comparison != 0) {
			# if different, achtung (even an update requires an option to do it)
			if ($comparison > 0) {
				say 'mappings differ but can be updated [', $comparison, ']';
			} else {
				die 'mappings differ and can NOT be updated [', $comparison, ']';
			}

			say 'updating the mapping';

			# if not exists, create it
			$es->indices->put_mapping({
				index => $index,
				type => $type,
				body => $dst_mappings->{$type}
			}) or die 'failed to update mapping for [index:', $index, '][type:', $type, ']';
		} else {
			say 'mapping matches, nothing to do';
		}
	}
}

#===============================================================================
sub alias {
#===============================================================================
	my $op = shift;

	die 'usage error: alias action is required' unless defined $op;

	if ($op =~ /^(del|rem|delete|remove)$/) {
		delete_alias(@_);
	} elsif ($op =~ /^(move)$/) {
		move_alias(@_);
	} elsif ($op =~ /^(add|put|create)$/) {
		add_alias(@_);
	} else {
		die 'usage error: invalid alias action';
	}
}

#===============================================================================
sub move_alias {
#===============================================================================
	my $src = $opts->{'src-index'};
	my $dst = $opts->{'dst-index'};
	my $alias = $opts->{'alias'};

	die 'alias queries is not yet supported' if defined $opts->{'query'};

	die 'usage error: missing src-index' unless defined $src;
	die 'usage error: missing dst-index' unless defined $dst;
	die 'usage error: missing alias' unless defined $alias;

	my $request_body = {
		actions => [
			{ remove => { alias => $alias, index => $src }},
			{ add    => { alias => $alias, index => $dst }},
		]
	};

	say ">>> request: \n", to_pretty_json($request_body) if $opts->{verbose};

	my $response = $es->indices->update_aliases(body => $request_body);

	die 'failed to move alias [', $alias, '] from index [', $src, '] to index [', $dst, ']' unless $response;

	say "<<< response: \n", to_pretty_json($response) if $opts->{verbose};
}

#===============================================================================
sub add_alias {
#===============================================================================
	my $index = $opts->{'dst-index'} || $opts->{'index'};
	my $alias = $opts->{'alias'};
	my $query = $opts->{'query'};

	# TODO: add query support
	die 'alias queries is not yet supported' if defined $query;

	die 'usage error: missing index' unless defined $index;
	die 'usage error: missing alias' unless defined $alias;

	my $response = $es->indices->put_alias(
		index => $index,
		name => $alias,
		#body => $query,
	);

	die 'failed to create alias [', $alias, '] for index [', $index, ']' unless $response;

	say to_pretty_json($response);
}

#===============================================================================
sub delete_alias {
#===============================================================================
	my $index = $opts->{'src-index'} || $opts->{'index'};
	my $alias = $opts->{'alias'};

	die 'usage error: missing index' unless defined $index;
	die 'usage error: missing alias' unless defined $alias;

	my $response = $es->indices->delete_alias(
		index => $index,
		name => $alias,
	);

	die 'failed to delete alias [', $alias, '] for index [', $index, ']' unless $response;

	say to_pretty_json($response);
}

#===============================================================================
sub mappings_comparator {
#===============================================================================
	my $src = shift;
	my $dst = shift;

	return 3 unless defined $src;

	my @sv = split /\./, $src;
	my @dv = split /\./, $dst;

	# major version must be the same, otherwise we return -1 which means incompatible majors
	return -1 if $sv[0] != $dv[0];
	# we return 2 if the minor version of the destination is an upgrade
	return 2 if $sv[0] == $dv[0] and $sv[1] < $dv[1];
	# we return 1 if the patch version of the destination is an upgrade
	return 1 if $sv[1] == $dv[1] and $sv[2] < $dv[2];
	# if the version is exactly the same, we return 0
	return 0 if $sv[1] == $dv[1] and $sv[2] == $dv[2];

	return -2;
}

#===============================================================================
sub to_pretty_json {
#===============================================================================
	return JSON::XS->new->ascii->pretty->allow_nonref->encode(shift);
}

#===============================================================================
sub to_json {
#===============================================================================
	return JSON::XS->new->ascii->allow_nonref->encode(shift);
}

__END__

=head1 NAME

ESA - ElasticSearch Administration

=head1 VERSION

Version 0.2.0

=head1 DESCRIPTION

A command line lib and tool for managing elasticsearch indices. The main features are

=over 4

=item View indice settings

=item Change indice settings

=item Backup an indice into JSON files

=item Restore a backup

=item Restore a backup into a different index and/or type

=item Reindex all documents or all documents of a given type into the same index or a different index

=item Supports plugins which can be used to change documents before they are indexes when using the restore or reindex commands

=back

=head1 SYNOPSIS

=head2 Snapshots

B<TODO>

=head2 Change index settings

  esa [OPTIONS] set --index <src index> --no-writes --replicas 2 --refresh-interval -1
  esa [OPTIONS] set --index <src index> --writes --replicas 1 --refresh-interval 1

=head2 Create indexes and types

  esa [OPTIONS] create  --index <index> --mfolder <path> [--type <type>]

The C<mfolder> path must be a folder containing a C<mappings.json> and C<settings.json>
files. Alternativelly it can also be a single JSON file with both settings and mappings.

TODO: add link to mappings and settings example files.

=head2 Backup and Restore to a different index

  esa [OPTIONS] backup  --index <src index> --folder <path> [--type <type>]
  esa [OPTIONS] restore --src-index <src index> --dst-index <dst index> --folder <path> [--type <type>] [--plugin <plugin>]

=head2 Reindex without Backup

  esa [OPTIONS] reindex --src-index <src index> --dst-index <dst index> [--type <type>] [--dst-host <host>]

=head2 Reindex to the same index using a plugin to change documents (DataRepair)

  esa [OPTIONS] reindex --index <index> --plugin ESA_main.pl [--type <type>] [--dst-host <host>]

=head2 Alias Management

  esa [OPTIONS] alias del --index <old index> --alias <alias>
  esa [OPTIONS] alias put --index <new index> --alias <alias> [--query <json query>]
  esa [OPTIONS] alias move --src-index <old index> --dst-index <new index> --alias <alias> [--query <json query>]

Note that you can not index against aliases which point to more than one index or which have queries associated. So if you are using aliases to version indices keep that in mind.

=head1 OPTIONS

=head2 --host <host>

The elasticsearch host to connect to. It must be an HTTP interface but you can omit the full URL. For example: C<--host localhost:9200>. You can provide full HTTP URI such as C<https://user:pass@hostname:9200>.

=head2 --verbose

This flag activates verbose mode.

=head2 --man

The full manual page.

=head2 --help

A quick help message.

=head2 --ignore-conflict

When re-indexing or restoring a backup, pass this option to ignore conflicts. A Warning is still displayed with a verbose message informing of what went wrong.

=head2 --index <index name>

This "option" is required by most actions and it indicates the index where to do the requested action.

=head2 --src-index <source index>

This "option" is required by some actions when there is the notion of an index which is either being replaced or removed. In some actions, which use only one index, sometimes support passing this options instead of C<--index>. For example, doing a backup of an index.

=head2 --dst-index <destination index>

Same as the C<--src-index>, this option is used to indicate an index which is either being created or updated.

=head2 --type <index type>

This option indicates the Elasticsearch document type.

=head2 --from <file index>

Used by the restore command. Allows starting at a specific file number from the backup files. Backup files have the format <index>[-<type>]-XXXXX.json where the XXXXX is a 0 padded number.

=head2 --mfolder <path to folder or file>

The mappings folder must point to a folder or file. If pointing to a folder it must contain 2 files: C<settings.json> and C<mappings.json>. The first must contain an index settings description. The C<mappings.json> must contain all types and mappings.

Sample C<settings.json>

  {
    "number_of_shards": 5,
    "index": {
      "analysis": {
        "analyzer": {
          "default": {
            "tokenizer": "standard",
            "filter": ["lowercase", "word_delimiter", "icu_folding"]
          },
          "myphonetica": {
            "tokenizer": "standard",
            "filter": ["standard", "lowercase", "myphonetic"]
          }
        },
        "filter": {
          "myphonetic": { "type": "phonetic", "encoder": "nysiis" }
        }
      }
    }
  }

Sample C<mappings.json>

  {
    "user": {
      "_meta": { "version": "0.1.0" },
      "properties": {
        "username": { "type": "string" }
      }
    },
    "tweet": {
      "_meta": { "version": "0.1.0" },
      "_parent": { "type": "user" },
      "properties": {
        "text":  { "type": "string" },
        "timestamp": { "type": "date" }
      }
    }
  }

Alternativelly, the C<mfolder> path can point to a file. This file must be a JSON object containg both settings and all mappings:

  {
    "settings": {
      ...
    },
    "mappings": {
      ...
    }
  }


=head2 --plugin <plugin>

See the plugins section.

=head2 --query <json query>

This option, used by the alias management, is used to provide queries to filter index documents. See the appropriate Elasticsearch documentation on how to use them.

=head1 Plugins

ESA supports the use of plugins in order to change documents before an indexing
operation. All plugins must be in Perl and must export an C<ESA_main> sub. This
function receives two arguments, a C<meta> hashref and a C<doc> hashref

Sample C<meta>

  {
    _index => 'index',
    _type => 'type',
    _id => 'doc id',
  }

The C<doc> is the document stored in elasticsearch (what is stored in the
C<_source>).

The C<ESA_main> can change the C<doc> hashref as it sees fit and it must return
an integer value with the following meaning:

=over 4

=item If it is less than 0 the document is invalid and should not be indexed

=item If it is equal to 0 then the document was not changed in any way, yet it
can still be indexed.

=item If it greater than 0 then the document was changed and the new version can
be indexed.

=back

Here is a quick C<esa-sample.pl> which convers a previous plain text field into
an MD5 of the same value.

=head2 esa-sample.pl

  use Digest::MD5 qw/md5_hex/;
  sub ESA_main {
    my ($meta, $doc) = @_;
    my $r = 0;
    unless (defined $doc->{'new-field'}) {
      $doc->{'new-field'} = md5sum($doc->{'old-field'});
      delete $doc->{'old-field'};
      $r = 1; # we changed the document so we set the output to 1
    }
    return -1 if $doc->{'invalid'};
    return $r;
  }
  1; # requires are interpreted, so we need an exit value ;)

=head1 INSTALL

Recommended is to use CPANMinus. To install CPANMinus, as root, do:

  curl -L http://cpanmin.us | perl - --sudo App::cpanminus

After that clone ESA and in its root folder type:

  sudo cpanm .

That's it.

=head2 Ubuntu/Debian

You need some extra packages which probably don't come with your dist.

  apt-get install libssl-dev

=head1 AUTHORS

J.B. Ribeiro, E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT

Copyright (C) 2015, EerieSoftronics

=cut
